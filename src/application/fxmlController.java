package application;

import javax.swing.JOptionPane;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import main.KeyMaker;

public class fxmlController {
	
	@FXML Button btn_konvertieren;
	@FXML Button btn_exportieren;
	@FXML Button btn_test;
	@FXML TextArea ta_originalText;
	@FXML TextArea ta_verschluesselt;
	@FXML TextArea ta_ascii;
	@FXML TextField tf_key;
	@FXML Text t_sonderzeichen;
	@FXML TextArea ta_ascii_original;
	
	
	public void initialize(){
		btn_konvertieren.setDisable(true);
		btn_exportieren.setDisable(true);
		btn_test.setVisible(false);
		ta_verschluesselt.setEditable(false);
		ta_ascii.setEditable(false);
		t_sonderzeichen.setVisible(false);
		ta_originalText.setWrapText(true);
		ta_verschluesselt.setWrapText(true);
		ta_ascii.setWrapText(true);
		ta_ascii_original.setWrapText(true);
		ta_ascii_original.setEditable(false);

	}
	
	public void konvertiereText() throws InterruptedException {
		// Pr�fen ob der Input OK ist, wenn nicht --> OriginalText in TextArea
		// 'ta_originalText' anpassen
		String klartext = ta_originalText.getText();
		String klartextBereinigt = KeyMaker.pruefeKlarText(klartext);
		if (!klartextBereinigt.equals(klartext))
			ta_originalText.setText(klartextBereinigt);

		//Pr�fen ob keine falschen Zeichen in der Passphrase eingegeben wurden.
		if (pruefeEingabePassphraseVorKonvertierung()) {
			t_sonderzeichen.setVisible(true);
			JOptionPane.showMessageDialog(null, "Sonderzeichen und Leerzeichen sind in Passphrase nicht erlaubt!");
		} else {
			// TextFeld 'Verschl�sselt' abf�llem mit dem verschl�sselten Text
			String textVerschluesselt = KeyMaker.verschluessle(ta_originalText.getText(), tf_key.getText());
			ta_verschluesselt.setText(textVerschluesselt);

			// Textfeld 'ASCII' mit dem ASCII-Wert der Verschl�sselung abf�llen
			StringBuffer ascii = KeyMaker.convertToASCII(textVerschluesselt);
			ta_ascii.setText(ascii.toString());
			
			// Textfeld 'ASCII original' wird mit den Ascii Werten abgef�llt
			StringBuffer ascii_origText = KeyMaker.convertToASCII(ta_originalText.getText());
			ta_ascii_original.setText(ascii_origText.toString());
		}

	}
	
	
	public void aktiviereButtons(){
		if(ta_originalText.getText().length() != 0 && tf_key.getText().length() != 0){
			btn_konvertieren.setDisable(false);
			btn_exportieren.setDisable(false);
		}
		else{
			btn_konvertieren.setDisable(true);
			btn_exportieren.setDisable(true);
		}
	}
	
	
	public void pruefeEingabePassphrase() throws InterruptedException{
		String check = tf_key.getText();
		char checkChar = check.charAt(check.length()-1);
		if(!Character.isAlphabetic(checkChar) || (int)checkChar == 228 /*�*/ || (int)checkChar == 246 /*�*/ || (int)checkChar == 252 /*�*/){
			tf_key.deletePreviousChar();
			t_sonderzeichen.setVisible(true);
		}

		
	}
	
	public boolean pruefeEingabePassphraseVorKonvertierung(){
		t_sonderzeichen.setVisible(false);
		String check = tf_key.getText();
		int countFailer = 0;
		boolean isFail = false;
		
		for (int i = 0; i < check.length()-1; i++) {
			char checkChar = check.charAt(i);
			if(!Character.isAlphabetic(checkChar) || (int)checkChar == 228 /*�*/ || (int)checkChar == 246 /*�*/ || (int)checkChar == 252 /*�*/){
				countFailer++;
//				System.out.println(countFailer);
			}
		}
		if(countFailer > 0){
			isFail = true;
		}
			
		return isFail;
	}
	
	public void exportiere(){
		JOptionPane.showMessageDialog(null, "Diese Funktion folgt, sobald Adriano die Logik liefert :-)");
		
	}
	
	
	public void testmethode(){
		System.out.println("Juhui");
	}
	
	
	
}
