package main;

import java.util.HashMap;
import java.util.Map;

import javax.swing.JOptionPane;

public class KeyMaker {

//	public static void main (String[] args){
//		String out = pruefeKlarText("!�� �b! bb");
//		System.out.println(out);
//		
//	}
	
	
	/** Pr�ft, ob der Input OK ist und passt den Klartext gegebenfalls an. Entfernt Sonderzeichen (ausser Leerzeichen) und ersetzt Umlaute.
	 * @param klartext
	 * @return Text ohne Sonderzeichen und Umlaute.
	 */
	public static String pruefeKlarText(String klartext){
		int fehler = 0;
		// Meldung ausgeben wenn Umlaute oder Sonderzeichen im Text enthalten sind
		for (int i = 0; i < klartext.length(); i++) {
			char charCheck = klartext.charAt(i);
			if (!Character.isAlphabetic(charCheck) && !Character.isWhitespace(charCheck)
					|| (int) charCheck == 228 /* � */ || (int) charCheck == 246 /* � */
					|| (int) charCheck == 252 /* � */ || (int) charCheck == 9 /* Tab */ || (int) charCheck == 10 /* Enter */)
				fehler++;
		}
		
		if(fehler > 0)
			JOptionPane.showMessageDialog(null, "Keine Sonderzeichen und Umlaute im Originaltext erlaubt. Der Text wird angepasst!");
		
		//Umlaute durch zwei Buchstaben ersetzen
		klartext = klartext.replaceAll("�", "ae");
		klartext = klartext.replaceAll("�", "oe");
		klartext = klartext.replaceAll("�", "ue");
		klartext = klartext.replaceAll("\n", " ");
		klartext = klartext.replaceAll("\t", " ");
		
		//Sonderzeichen entfernen (leerzeichen bleiben bestehen)
		StringBuffer klartextOhneSonderZeichen = new StringBuffer();
		
//		System.out.println(klartext);
		for (int i = 0; i < klartext.length(); i++) {
//			System.out.println(Character.isAlphabetic(klartext.charAt(i)));
			if(Character.isAlphabetic(klartext.charAt(i)) == true || Character.isWhitespace(klartext.charAt(i))== true){
				klartextOhneSonderZeichen.append(klartext.charAt(i));
			}
		}
		String out = klartextOhneSonderZeichen.toString().toUpperCase();
		
		return out;
	}
	
	
	
	/**
	 * Nimmt einen Klartext sowie einen Schl�ssel entgegen und verschl�sselt so
	 * anhand der Vigen�re Methode
	 * 
	 * @param klarText
	 *            Text den man verschl�sseln m�chte
	 * @param schluessel
	 *            Die Passphrase mit der verschl�sselt wird
	 * @return die verschl�sselte Botschaft als String
	 */
	public static String verschluessle(String klarText, String schluessel) {
		
		//Pr�fen ob der Input OK ist
		String klartextBereinigt = pruefeKlarText(klarText);
		
		
		// Das Vigen�re Quadrat gem. Wikipedia:
		Character[][] vQuadrat = {
				{'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'},
				{'B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','A'},
				{'C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','A','B'},
				{'D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','A','B','C'},
				{'E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','A','B','C','D'},
				{'F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','A','B','C','D','E'},
				{'G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','A','B','C','D','E','F'},
				{'H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','A','B','C','D','E','F','G'},
				{'I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','A','B','C','D','E','F','G','H'},
				{'J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','A','B','C','D','E','F','G','H','I'},
				{'K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','A','B','C','D','E','F','G','H','I','J'},
				{'L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','A','B','C','D','E','F','G','H','I','J','K'},
				{'M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','A','B','C','D','E','F','G','H','I','J','K','L'},
				{'N','O','P','Q','R','S','T','U','V','W','X','Y','Z','A','B','C','D','E','F','G','H','I','J','K','L','M'},
				{'O','P','Q','R','S','T','U','V','W','X','Y','Z','A','B','C','D','E','F','G','H','I','J','K','L','M','N'},
				{'P','Q','R','S','T','U','V','W','X','Y','Z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O'},
				{'Q','R','S','T','U','V','W','X','Y','Z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P'},
				{'R','S','T','U','V','W','X','Y','Z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q'},
				{'S','T','U','V','W','X','Y','Z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R'},
				{'T','U','V','W','X','Y','Z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S'},
				{'U','V','W','X','Y','Z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T'},
				{'V','W','X','Y','Z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U'},
				{'W','X','Y','Z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V'},
				{'X','Y','Z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W'},
				{'Y','Z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X'},
				{'Z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y'},
		};
		
		// HashMap erstellen, um Zeile und Spalte bestimmen zu k�nnen f�r das Auslesen in der vQuadrat-Tabelle
		Map<Character,Integer> alphabet = new HashMap<>();
		for (int i = 0; i < vQuadrat.length; i++) {
			alphabet.put(vQuadrat[0][i],i);
		}
		
		
		int schluesselZaehler = 0;
		StringBuffer verschluesselung = new StringBuffer();
		
		for (int i = 0; i < klartextBereinigt.length(); i++) {
			// Spalte bestimmen anhand des Klartextes
			char klarTextChar = Character.toUpperCase(klartextBereinigt.charAt(i));

			// Leerschl�ge werden nicht verschl�sselt (32 = Ascicode Leerschlag)
			if ((int) klarTextChar == 32) {
				verschluesselung.append(" ");
			} else {
				// System.out.print(alphabet.get(klarTextChar));
				int spalte = alphabet.get(klarTextChar);

				// Zeile bestimmen anhand des Schl�ssels
				char schluesselChar = Character.toUpperCase(schluessel.charAt(schluesselZaehler));
				// System.out.println(alphabet.get(schluesselChar));
				int zeile = alphabet.get(schluesselChar);
				schluesselZaehler++;
				if (schluesselZaehler == schluessel.length()) {
					schluesselZaehler = 0;
					// System.out.println("Schl�sselwort beginnt neu");
				}

				// Verschl�sselung anhand vQuadrat
				verschluesselung.append(vQuadrat[zeile][spalte]);
			}
		}
		
		String out = verschluesselung.toString();
		return out; 
		
		
	}

	
	/**
	 * Wandelt einen originaltext in die entsprechenden ASCII Zeichen um.
	 * 
	 * @param originalText
	 *            Ein Originaltext als String
	 * @return Ein Stringbuffer mit den ASII Zeichen aneinandergereiht (mit
	 *         leerzeichen dazwischen)
	 */
	public static StringBuffer convertToASCII(String originalText) {
		StringBuffer ascii = new StringBuffer();

		for (int i = 0; i < originalText.length(); i++) {
			int charachter = (int) originalText.charAt(i);
			if(charachter < 100)
				ascii.append(0);
			
			ascii.append(charachter);
			ascii.append(" ");
		}

		return ascii;
	}

}
